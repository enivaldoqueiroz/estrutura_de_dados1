Exercicios de Estrutu de Dados I


1) Faça um algoritmo que leia dois números A e B e imprima o maior deles.

2) O sistema de avaliação de determinada disciplina, é composto por três provas. A primeira prova tem peso 2, a segunda tem peso 3 e a terceira tem peso 5. Faça um algoritmo para calcular a média final de um aluno desta disciplina e avaliar se ele foi aprovado por media. A media final deve ser superior a 7.

3) Uma empresa produz três tipos de peças mecânicas: parafusos, porcas e arruelas. Têm-se os preços unitários de cada tipo de peça e sabe-se que sobre estes preços incidem descontos de 10% para porcas, 20% para parafusos e 30% para arruelas. Escreva um algoritmo que calcule o valor total da compra de um cliente. Deve ser mostrado o nome do cliente. O número de cada tipo de peça que o mesmo comprou, o total de desconto e o total a pagar pela compra.

4) Elaborar um algoritmo que lê dois valores a e b e os escreve com a mensagem: “São múltiplos” ou “Não são múltiplos”.

5) Faça um algoritmo que leia a primeira letra do estado civil de uma pessoa e mostre uma mensagem com a sua descrição (Solteiro, Casado, Viúvo, Divorciado, Desquitado). Mostre uma mensagem de erro, se necessário.

6) Crie um algoritmo que solicita ao usuário para digitar um número e
mostra-o por extenso. Este número deve variar entre 1 e 10. Se o usuário
introduzir um número que não está neste intervalo, mostre: "Número
inválido".
